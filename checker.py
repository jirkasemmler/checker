import sys
import requests
import re
import urlparse
from bs4 import BeautifulSoup
from pprint import pprint
import time
import os
import json
import glob
import sys
sys.setrecursionlimit(100000) # 10000 is an example, try with different values

class FileManager:

    workingPath = "./"
    domainsDir = "domains"
    urlDir = ""

    def __init__(self, url):
        self.url = str(url)
        self.urlDir = self.url.replace("/", "__")

    def prepare_dir(self):
        if not os.access(self.workingPath + self.domainsDir, os.F_OK):
            try:
                os.mkdir(self.workingPath+ self.domainsDir)
            except:
                print "unable to create working directory"
                return False
        if not os.access(self.workingPath + self.domainsDir, os.W_OK):
            print "unable to write working directory"
            return False

        if not os.access(self.workingPath + self.domainsDir + "/" + self.urlDir, os.F_OK):
            try:
                os.mkdir(self.workingPath + self.domainsDir + "/" + self.urlDir)
            except:
                print "unable to create the domain directory"
                return False
        if not os.access(self.workingPath + self.domainsDir + "/" + self.urlDir, os.W_OK):
            print "unable to write to the domain directory"
            return False
        return True

    def write_output(self, data):
        self.prepare_dir()

        file_name = "out_" + time.strftime("%Y-%m-%d_%H-%M-%S") + ".json"

        with open(self.workingPath + self.domainsDir + "/" + self.urlDir + "/" + file_name, "w+") as fout:
           json.dump(data, fout, indent=4)
        print "result stored in " + self.workingPath + self.domainsDir + "/" + self.urlDir + "/" + file_name
        return self.workingPath + self.domainsDir + "/" + self.urlDir + "/" + file_name
        # json.dump(data, open(file_name, 'w'))

    def get_version_by(self, date):
        if not os.access(self.workingPath + self.domainsDir + "/" + self.urlDir + "/out_" + date + ".json", os.R_OK):
            print "unable to read file to compare"
            return False
        return self.workingPath + self.domainsDir + "/" + self.urlDir + "/out_" + date + ".json"

    def get_last_version(self):
        print self.workingPath + self.domainsDir + "/" + self.urlDir

        newest = max(glob.iglob(self.workingPath + self.domainsDir + "/" + self.urlDir + "/*"), key=os.path.getctime)
        return newest

    def compare(self, this_version, compared_version):

        with open(this_version) as data_file:
            this_data = json.load(data_file)

        with open(compared_version) as data_file:
            compared_file = json.load(data_file)

        this_links = this_data['links']
        this_out_links = this_links.copy()
        compared_links = compared_file['links']
        for item in this_links:
            if item in compared_links.keys() and this_links[item] == compared_links[item]:
                del this_out_links[item]
                del compared_links[item]


        if len(this_out_links) == 0 and len(compared_links) == 0:
            print "results are identical"
        else:
            print "different results"
            print "BEFORE"
            pprint(compared_links)
            print "-------"
            print "AFTER"
            pprint(this_out_links)


class Checker:
    main_url = ""
    main_domain = ""
    usedLinks = []
    links = {}
    redirectLocations = []  # list (code , url)
    redirectUrls = []  # list locations
    template = {'code': 0, 'data': None}
    counters = {'total': 0}
    followImgs = False


    def __init__(self, url):
        self.main_url = url
        self.main_domain = urlparse.urljoin(url, '/')
        print self.main_domain
        print ""

    def recount(self, code):
        self.counters['total'] += 1
        if code in self.counters.keys():
            self.counters[code] += 1
        else:
            self.counters[code] = 1

    def get_data(self, url=None):

        if url is None:
            url = self.main_url
        try:
            r = requests.get(url, allow_redirects=False)
        except:
            print url + ": Exception"
            return False

        self.usedLinks.append(url)
        print url + ":" + str(r.status_code)
        if self.is_leaving(url):
            self.make_entry(url, r.status_code, None)

        elif r.is_redirect or r.is_permanent_redirect:
            is_cycle = self.try_cycle(url)  # request or None
            if is_cycle is not None:  # neni tam cyklus
                # self.getLinks(isCycle.content)
                code = r.status_code
                self.get_data(r.headers['Location'])
            else:
                code = 'redirect_loop'
            self.make_entry(url, code, self.redirectLocations)

        else:
            self.make_entry(url, r.status_code, None)
            self.get_links(r.content)

    def make_entry(self, link, code, data):

        template = self.template.copy()

        template['code'] = code
        template['data'] = data
        self.links[link] = template

        # print link + " : " + str(code)
        self.recount(code)
        # if data is not None:
            # print data

    def is_leaving(self, link):
        return not re.match(self.main_url + ".*", link)

    def try_cycle(self, url):
        self.redirectLocations = []  # vynuluju
        self.redirectUrls = []  # vynuluju
        res = self.make_step(url)

        return res

    def make_step(self, url):
        r = requests.get(url, allow_redirects=False)
        if r.is_redirect or r.is_permanent_redirect:

            if r.headers['Location'] in self.redirectUrls:
                return None
            self.redirectUrls.append(r.headers['Location'])
            self.redirectLocations.append((r.status_code, r.headers['Location']))
            return self.make_step(r.headers['Location'])
        else:
            return r

    def get_absolute_path(self, link):
        if link[0] is "/":
            return self.main_url + link[1:]
        else:
            return self.main_url + link

    def follow_img(self, link):
        if self.followImgs:
            return True
        else:

            tmp = link.split(".")
            if tmp.__len__() > 0 and str(tmp[-1]).lower() in ["jpg", "jpeg", "png", "gif"]:
                return False
            return True

    def get_links(self, data):
        soup = BeautifulSoup(data, "lxml")
        links = soup.find_all('a')

        for tag in links:
            link = tag.get('href', None)

            if link not in [None, ""] and self.follow_img(link):
                if self.is_valid_url(link):

                    if not self.is_absolute(link) and self.get_absolute_path(link) not in self.usedLinks:
                        self.get_data(self.get_absolute_path(link))
                    elif self.is_absolute(link) and link not in self.usedLinks:
                        self.get_data(link)

    @staticmethod
    def is_absolute(url):
        return bool(urlparse.urlparse(url).netloc)

    @staticmethod
    def is_valid_url(link):
        return not re.match(".*(mailto|tel|javascript|\.zip|\.exe):.*", link) and not re.match(".*(\.zip|\.exe|\.iso|\.msi|\.rar)$", link)


if sys.argv.__len__() >= 2:

    if bool(urlparse.urlparse(sys.argv[1]).netloc) and sys.argv[1][-1] is "/":
        checker = Checker(sys.argv[1])
        checker.get_data()

        out = {'domain': sys.argv[1],
               'date': time.strftime("%Y-%m-%d %H:%M:%S"),
               'totals': checker.counters,
               'links': checker.links
               }
        filemanager = FileManager(sys.argv[1])
        if sys.argv.__len__() == 2:
            filemanager.write_output(out)

        elif sys.argv.__len__() == 3 and sys.argv[2] == "-s":
            pprint(out)

        elif sys.argv.__len__() >= 3 and sys.argv[2] == "-c":

            if sys.argv.__len__() == 4:
                to_compare_with = filemanager.get_version_by(sys.argv[3])
            else:
                to_compare_with = filemanager.get_last_version()

            this_file = filemanager.write_output(out)
            if to_compare_with is not None:
                filemanager.compare(this_file, to_compare_with)
    elif sys.argv[1] in ["-h", "--help"]:
        print "set absolute path http://<domain>.cz/"
        print "usage: python checker.py http(s)://example.com/directory/ -s|-c|-c <datetime>|<nothing>"
        print ""
        print "-s           : print only the result to stdout, doesnt write result to file"
        print "-c           : writes result to a file and compares with the last version"
        print "-c <datetime>: writes result to a file and compares with selected version"
        print "<nothing>    : writes result to a file"
    else:
        print "use python checker.py --help"
else:
    print "use python checker.py --help"
