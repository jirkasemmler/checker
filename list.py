import sys
import requests
import re
import urlparse
from bs4 import BeautifulSoup
from pprint import pprint
import time
import os
import json
import glob

with open("urls") as f:
    content = f.read().splitlines()
    for item in content:
        try:
            r = requests.get("http://"+item+"/")
            print item + " : " + str(r.status_code)
        except:
            print item + " : ERROR"
